{
  imports = [
    ./plugins

    ./auto-commands.nix
    ./options.nix
  ];

  config = {
    colorschemes.dracula.enable = true;

    clipboard.providers = {
      xclip.enable = true;
      xsel.enable = true;
    };
  };
}
