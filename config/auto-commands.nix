{
  autoCmd = [
    # Yank highlight
    {
      event = ["TextYankPost"];
      pattern = ["*"];
      command = "lua vim.highlight.on_yank {higroup='IncSearch', timeout=350}";
    }
    # Toggle relativenumber when changing to/from command mode
    {
      event = ["CmdlineEnter"];
      pattern = ["*"];
      command = "set norelativenumber|redraw";
    }
    {
      event = ["CmdlineLeave"];
      pattern = ["*"];
      command = "set relativenumber";
    }
    # Enable spell checking for certain file types
    {
      event = ["BufRead" "BufNewFile"];
      pattern = ["*.txt" "*.md" "*.tex"];
      command = "setlocal spell";
    }
  ];
}
