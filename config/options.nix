{
  config = {
    options = {
      autoindent = true;
      autoread = true;
      background = "dark";
      cmdheight = 4;
      confirm = true;
      encoding = "utf8";
      expandtab = true;
      fileformat = "unix";
      hidden = true;
      hlsearch = true;
      ignorecase = true;
      inccommand = "nosplit";
      incsearch = true;
      linebreak = true;
      number = true;
      relativenumber = true;
      scrolloff = 5;
      shiftround = true;
      shiftwidth = 2;
      showcmd = true;
      showmode = false;
      sidescrolloff = 5;
      smartcase = true;
      smartindent = true;
      smarttab = true;
      spell = false;
      spelllang = "en,de";
      tabstop = 2;
      termguicolors = true;
      timeoutlen = 100;
      undofile = true;
      updatetime = 0;
      wildmenu = true;
      wrap = true;
    };

    globals.mapleader = " ";
  };
}
