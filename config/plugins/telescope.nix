{
  config = let
    keyPrefix = "<leader>t";
  in {
    plugins.telescope.enable = true;

    plugins.which-key.registrations = {
      ${keyPrefix} = "+Telescope";
      "${keyPrefix}l" = "+Lsp";
      "${keyPrefix}lw" = "+Workspace";
      "${keyPrefix}ld" = "+Document";
    };

    keymaps = [
      {
        # Show hidden files but ignore .git directory
        action = "<Cmd>lua require'telescope.builtin'.find_files({ find_command = {'rg', '--files', '--hidden', '--glob', '!**/.git/*'} })<CR>";
        key = "${keyPrefix}" + "p";
        options = {
          desc = "files";
          silent = true;
        };
      }
      {
        action = "<cmd>Telescope live_grep<CR>";
        key = "${keyPrefix}" + "g";
        options = {
          desc = "live grep";
          silent = true;
        };
      }
      {
        action = "<Cmd>Cheat<CR>";
        key = "${keyPrefix}" + "C";
        options = {
          desc = "cheatsheet";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope media_files<CR>";
        key = "${keyPrefix}" + "M";
        options = {
          desc = "media files";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope spell_suggest<CR>";
        key = "${keyPrefix}" + "S";
        options = {
          desc = "spell";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope spell_suggest<CR>";
        key = "${keyPrefix}" + "S";
        options = {
          desc = "spell";
          silent = true;
        };
      }
      {
        action = "<Cmd>lua require('telescope').extensions.asynctasks.all()<CR>";
        key = "${keyPrefix}" + "T";
        options = {
          desc = "tasks";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope<CR>";
        key = "${keyPrefix}" + "a";
        options = {
          desc = "all";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope file_browser<CR>";
        key = "${keyPrefix}" + "b";
        options = {
          desc = "file browser";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope buffers<CR>";
        key = "${keyPrefix}" + "B";
        options = {
          desc = "buffers";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope current_buffer_fuzzy_find<CR>";
        key = "${keyPrefix}" + "f";
        options = {
          desc = "fuzzy find buffer";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope help_tags<CR>";
        key = "${keyPrefix}" + "h";
        options = {
          desc = "help tags";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope jumplist<CR>";
        key = "${keyPrefix}" + "j";
        options = {
          desc = "jumplist";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope keymaps<CR>";
        key = "${keyPrefix}" + "k";
        options = {
          desc = "keymaps";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope marks<CR>";
        key = "${keyPrefix}" + "m";
        options = {
          desc = "marks";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope project<CR>";
        key = "${keyPrefix}" + "P";
        options = {
          desc = "projects";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope quickfix<CR>";
        key = "${keyPrefix}" + "q";
        options = {
          desc = "quickfix";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope oldfiles<CR>";
        key = "${keyPrefix}" + "r";
        options = {
          desc = "recent files";
          silent = true;
        };
      }
      {
        action = "<Cmd>TodoTelescope<CR>";
        key = "${keyPrefix}" + "t";
        options = {
          desc = "todo";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope live_grep<CR>";
        key = "${keyPrefix}" + "s";
        options = {
          desc = "live grep";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_definitions<CR>";
        key = "${keyPrefix}l" + "D";
        options = {
          desc = "definitions";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_code_actions<CR>";
        key = "${keyPrefix}l" + "a";
        options = {
          desc = "code actions";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_implementations<CR>";
        key = "${keyPrefix}l" + "i";
        options = {
          desc = "implementations";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_references<CR>";
        key = "${keyPrefix}l" + "r";
        options = {
          desc = "references";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope diagnostics<CR>";
        key = "${keyPrefix}lw" + "d";
        options = {
          desc = "diagnostics";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_workspace_symbols<CR>";
        key = "${keyPrefix}lw" + "s";
        options = {
          desc = "symbols";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope diagnostics<CR>";
        key = "${keyPrefix}ld" + "d";
        options = {
          desc = "diagnostics";
          silent = true;
        };
      }
      {
        action = "<Cmd>Telescope lsp_document_symbols<CR>";
        key = "${keyPrefix}ld" + "s";
        options = {
          desc = "symbols";
          silent = true;
        };
      }
    ];
  };
}
