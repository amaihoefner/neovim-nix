{
  imports = [
    ./undotree.nix
    ./telescope.nix
    ./wilder.nix
  ];

  config = {
    keymaps = [
      {
        action = "<cmd>UndotreeToggle<CR>";
        key = "<leader>u";
        options = {
          desc = "toggle undo tree";
          silent = true;
        };
      }
    ];

    plugins = {
      lsp = {
        enable = true;
        servers = {
          ansiblels.enable = true;
          bashls.enable = true;
          ccls.enable = true;
          cmake.enable = true;
          dockerls.enable = true;
          jsonls.enable = true;
          lua-ls.enable = true;
          marksman.enable = true;
          pyright.enable = true;
          ruff-lsp.enable = true;
          rust-analyzer = {
            enable = true;
            installCargo = true;
            installRustc = true;
          };
          taplo.enable = true;
          yamlls.enable = true;
        };
      };
      cmp = {
        enable = true;
        settings = {
          sources = [
            {name = "nvim_lsp";}
            {name = "path";}
            {name = "buffer";}
          ];
        };
      };
      better-escape.enable = true;
      comment-nvim.enable = true;
      coverage = {
        enable = true;
        autoReload = true;
      };
      crates-nvim.enable = true;
      cursorline.enable = true;
      dap.enable = true;
      direnv.enable = true;
      gitsigns.enable = true;
      leap.enable = true;
      lspkind.enable = true;
      lsp-lines.enable = true;
      luasnip.enable = true;
      nix.enable = true;
      notify.enable = true;
      nvim-colorizer.enable = true;
      nvim-lightbulb.enable = true;
      rust-tools.enable = true;
      surround.enable = true;
      todo-comments.enable = true;
      treesitter.enable = true;
      trouble.enable = true;
      which-key.enable = true;
    };
  };
}
